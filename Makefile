CC = gcc
CFLAGS = -g -c -Wall
OBJS = build/dclust.o build/main.o

all : main 


# Build and compile executables
 
main : main.o
	$(CC) -o bin/dclust $(OBJS) -Llib/ -lgsl -lgslcblas -lm -lz -lpthread
	rm -rf build


# Compile shared objects

main.o : dclust.o
	$(CC) $(CFLAGS) src/main.c -Isrc/include -o build/main.o

dclust.o : setup
	$(CC) $(CFLAGS) src/dclust.c -Isrc/include -o build/dclust.o


# Prepare build environment

setup:
	mkdir -p build
	mkdir -p bin

.PHONY : clean
clean :
	rm -rf bin
