#include <stdio.h>
#include <string.h>
#include <dclust.h>

#define MAX_ID_LENGTH 255
#define MAX_PATH_LENGTH 1000

#define HELP_MSG "\n\
dclust:\n\
  Clustering by fast search and find of density peaks with automatic distance cutoff calculation\n\n\
  [1] Rodriguez, A. and Laio, A. 2014. Science 344 (6191):1492-1496.\n\
  [2] Wang, S., Wang, D., Li, C. and Li, Y. 2015. arXiv:1501.04267.\n\n\
Usage:\n\
  dclust [OPTIONS] distance_file\n\n\
Options:\n\
  -g\n\
     Use gaussian kernel for density calculation\n\
  -d dc\n\
     Use dc as the distance cutoff for density calculation\n\
     If no -d option is set distance cutoff is calculated automatically as explained in [2]\n"


/*
 * Application entry point
 *
 * Usage:
 *   dclust [OPTIONS] distance_file number_elements
 * Options:
 *   -g : Use gaussian kernel
 *   -d : Set distance
 */
int main(int argc, char** argv)
{
  FILE* distancef;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  int n, i, j, nclusters, k, nlines;
  double** dist;
  int* cl;
  int* halo;
  char** ids;
  double dc = -1, maxd;
  int gaussian = 0;
  char carg;
  char distancefp[MAX_PATH_LENGTH];
  char ch[2][5] = {"CORE\0", "HALO\0"};
  char gk[2][4] = {"NO\0\0", "YES\0"};
  char* token;
  char c;

  // Parse command line
  opterr = 0;
  while((carg = getopt(argc, argv, "hgd:")) != -1) {
    switch (carg) {
      case 'h':
        fprintf(stderr, "%s\n", HELP_MSG);
        return 0;
      case 'g':
        gaussian = 1;
        break;
      case 'd':
        dc = atof(optarg);
        break;
      case '?':
        fprintf(stderr, "\nOption not recognized -%c\nType dclust -h for help\n\n", carg);
        return -1;
    }
  }
  if ((argc - optind) != 1) {
    fprintf(stderr, "\nInvalid number of arguments\nType dclust -h for help\n\n");
    return -1;
  }
  strncpy(distancefp, argv[argc - 1], MAX_PATH_LENGTH);

  // Read file and count number of lines
  nlines = 0;
  distancef = fopen(distancefp, "r");
  if (distancef == NULL) {
    fprintf(stderr, "\nFile %s does not exist or is not readable\n\n", distancefp);
    return -1;
  }
  while ((c = fgetc(distancef)) != EOF)
    if ( c == '\n' )
      nlines++;
  nlines++;
  fclose(distancef);

  // Calculate n
  n = (1 + sqrt(1 + 8 * nlines)) / 2;

  // Allocate matrix, halo, cl and ids;
  dist = (double**) malloc(n * sizeof(double*));
  ids = (char**) malloc(n * sizeof(char*));
  for (i = 0; i < n; i++) {
    dist[i] = (double*) malloc(n * sizeof(double));
    ids[i] = (char*) malloc(MAX_ID_LENGTH * sizeof(char));
  }

  // Read file and fill matrix
  distancef = fopen(distancefp, "r");
  for (i = 0; i < (n - 1); i++) {
    for (j = i + 1; j < n; j++) {
      read = getline(&line, &len, distancef);

      if (read < 0) {
        fprintf(stderr, "\nFile %s is ill-formatted\n\n", distancefp);
        if (line) free(line);
        for (k = 0; k < n; k++) {free(dist[i]); free(ids[i]);}
        free(ids); free(dist);
        return -1;
      }

      if ((token = strtok(line, "\t")) == NULL) {
        fprintf(stderr, "\nFile %s is ill-formatted\n\n", distancefp);
        if (line) free(line);
        for (k = 0; k < n; k++) {free(dist[i]); free(ids[i]);}
        free(ids); free(dist);
        return -1;
      }
      if (i == 0) strncpy(ids[i], token, MAX_ID_LENGTH);

      if ((token = strtok(NULL, "\t")) == NULL) {
        fprintf(stderr, "\nFile %s is ill-formatted\n\n", distancefp);
        if (line) free(line);
        for (k = 0; k < n; k++) {free(dist[i]); free(ids[i]);}
        free(ids); free(dist);
        return -1;
      }
      strncpy(ids[j], token, MAX_ID_LENGTH);

      if ((token = strtok(NULL, "\t")) == NULL) {
        fprintf(stderr, "\nFile %s is ill-formatted\n\n", distancefp);
        if (line) free(line);
        for (k = 0; k < n; k++) {free(dist[i]); free(ids[i]);}
        free(ids); free(dist);
        return -1;
      }
      dist[i][j] = atof(token);
      dist[j][i] = dist[i][j];
      dist[i][i] = 0.0;
      dist[j][j] = 0.0;
    }
  }
  fclose(distancef);
  if (line) free(line);

  // Calculate distance if needed
  if (dc < 0) {
    fprintf(stderr, "[LOG] Calculating distance\n");
    dc = dcoptimize(dist, n, &maxd);
  }

  // Calculate clusters
  fprintf(stderr, "[LOG] Performing clustering\n");
  fprintf(stderr, "[LOG]   Gaussian kernel : %s\n", gk[gaussian]);
  fprintf(stderr, "[LOG]   Distance cutoff : %f\n", dc);
  cl = (int*) malloc(n * sizeof(int));
  halo = (int*) malloc(n * sizeof(int));
  nclusters = dclust(dist, n, dc, gaussian, cl, halo);
  fprintf(stderr, "[LOG] Reporting results\n");
  fprintf(stderr, "[LOG]   %d clusters calculated\n", nclusters);

  // Report results
  for (i = 0; i < n; i++)
    fprintf(stdout, "%s\t%d\t%s\n", ids[i], cl[i], ch[halo[i]]);

  // Free matrix, halo, cl and ids
  free(cl);
  free(halo);
  for(i = 0; i < n; i++) {
    free(dist[i]);
    free(ids[i]);
  }
  free(dist);
  free(ids);

  // Return
  return 0;
}
