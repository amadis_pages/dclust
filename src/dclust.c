#include <dclust.h>

/*
 * cmpd
 *
 * @see include/dclust.h 
 */
int cmpd(const void *x, const void *y)
{
  double xx = *(double*)x, yy = *(double*)y;
  if (xx < yy) return -1;
  if (xx > yy) return  1;
  return 0;
}


/*
 * cmpsm
 *
 * @see include/dclust.h
 */
int cmpsm (const void *x, const void *y)
{
  rho_struct xx = *(rho_struct*) x;
  rho_struct yy = *(rho_struct*) y;

  if (xx.value < yy.value) return 1;
  if (xx.value > yy.value) return -1;
  return 0;
}


/*
 * dcoptimize
 *
 * @see include/dclust.h
 */
double dcoptimize(double** dist, int n, double* max)
{
  double dc, z, h, hmin, lower, upper, sigma;
  double* allds;
  int i, j, counter;
  double* potentials;

  // Initialize variables
  potentials = (double*) malloc(n * sizeof(double));
  allds = (double*) malloc(sizeof(double) * (n * (n - 1) / 2));
  hmin = DBL_MAX;

  // Convert dissimilarity matrix into a vector
  // Sort dissimilarity vector ascendently
  counter = 0;
  for (i = 0; i < n; i++) {
    for(j = i + 1; j < n; j++) {
      allds[counter] = dist[i][j];
      counter++;
    }
  }
  qsort(allds, n * (n - 1) / 2, sizeof(double), cmpd);
  *max = allds[counter - 1];

  // Calculate lower and upper boundaries for sigma (impact factor)
  //   lower : minimum distance > 0
  //   upper : distance at 10 percentile
  counter = 0;
  while(allds[counter] == 0) counter++;
  lower = allds[counter];
  upper = gsl_stats_quantile_from_sorted_data(allds, 1, (n * (n - 1) / 2), 0.10);

  // Calculate entropy for values of sigma between lower and upper in increments of 0.005
  for (sigma = lower; sigma <= upper; sigma += 0.005f) {

    // Calculate potential for each point
    for(i = 0; i < n; i++) {
      potentials[i] = 0.0f;
      for(j = 0; j < n; j++) {
        double sq = (dist[i][j] / sigma) * (dist[i][j] / sigma);
        double expsq = exp(-sq);
        if (j != i) potentials[i] += expsq;
      }
    }

    // Calculate Z (normalization factor)
    z = 0;
    for (i = 0; i < n; i++)
      z += potentials[i];

    // Calculate H (entropy)
    h = 0;
    for (i = 0; i < n; i++) {
      double a = potentials[i] / z;
      double b = log(a);
      h += a*b;
    }
    h = h * (-1);

    // Store dc and hmin
    if (h < hmin) {
      hmin = h;
      dc = sigma;
    }
  }

  free(potentials);
  free(allds);

  return((3/sqrt(2)) * dc);
}


/*
 * dclust
 *
 * @see include/dclust.h
 */
int dclust(double** dist, int n, double cutoff, int gaussian, int* cl, int* halo)
{
  double *rho, *delta, *sortrho, *sortdelta, *bord_rho;
  int i, j, nclust;
  double dc, maxd, rhothreshold, deltathreshold;
  rho_struct *strho;

  // Initialize structures
  rho = (double*) malloc(sizeof(double) * n);
  delta = (double*) malloc(sizeof(double) * n);
  sortrho = (double*) malloc(sizeof(double) * n);
  sortdelta = (double*) malloc(sizeof(double) * n);
  strho = (rho_struct*) malloc(sizeof(rho_struct) * n);

  // Calculate optimal dc
  // Calculate maximum distance
  dc = dcoptimize(dist, n, &maxd);
  if (cutoff > 0)
    dc = cutoff;

  // Initialize RHO
  for (i = 0; i < n; i++) rho[i] = 0.0;

  // Calculate RHO[i] per point using gaussian kernel
  //   RHO[i] = sum {exp(-(dist(i,j)/dc)^2)}
  if (gaussian) {
    for(i = 0; i < n; i++) {
      for(j = i + 1; j < n; j++) {
        double sq = -(dist[i][j] / (double)dc) * (dist[i][j] / (double)dc);
        double expsq = exp(sq);
        rho[i] += expsq;
        rho[j] += expsq;
      }
    }
  }

  // Calculate RHO per point
  //   RHO[i] = number of points j that satisfy dist(i,j) < dc
  if (!gaussian) {
    for(i = 0; i < n; i++) {
      for(j = i + 1; j < n; j++) {
        if(dist[i][j] < dc) {
          rho[i]++;
          rho[j]++;
        }
      }
    }
  }

  // Calculate DELTA per point
  //   DELTA[i] = minimum {dist(i,j) if RHO[j] > RHO[i]}
  for (i = 0; i < n; i++) {
    delta[i] = maxd;
    for (j = 0; j < n; j++) {
      if ((j != i) && (rho[i] < rho[j]) && (delta[i] > dist[i][j]))
        delta[i] = dist[i][j];
    }
  }

  // Calculation of RHO and DELTA threshold
  //   deltathreshold = 3rd quartile
  //   rhothreshold = 1st quartile 
  for (i = 0; i < n; i++) {
    sortrho[i] = rho[i];
    sortdelta[i] = delta[i];
  }
  qsort(sortrho, n, sizeof(double), cmpd);
  qsort(sortdelta, n, sizeof(double), cmpd);
  rhothreshold = gsl_stats_quantile_from_sorted_data(sortrho, 1, n, 0.25);
  deltathreshold = gsl_stats_quantile_from_sorted_data(sortdelta, 1, n, 0.75);

  // Identification of cluster centers
  //   i is a center <=> RHO[i] > rhothreshold and DELTA[i] > deltathreshold
  nclust = 0;
  for (i = 0; i < n; i++) {
    cl[i] = 0;
    if (rho[i] > rhothreshold && delta[i] > deltathreshold) 
      cl[i] = ++nclust;
  }

  // Order points according RHO values in ORDRHO descendently
  for (i = 0; i < n; i++) {
    strho[i].value = rho[i];
    strho[i].index = i;
  }
  qsort(strho, n, sizeof(rho_struct), cmpsm);

  // Assign clusters
  // Point is assigned to the same cluster as its nearest neighbor of higher density
  for (i = 0; i < n; i++) {
    int idxi = strho[i].index;
    if (cl[idxi] == 0) {
      double mindist = maxd;
      int minidx = n;
      for (j = 0; j < i; j++) {
        int idxj = strho[j].index;
        if ((rho[idxi] < rho[idxj]) && (dist[idxi][idxj] < mindist)) {
          cl[idxi] = cl[idxj];
          mindist = dist[idxi][idxj];
          minidx = idxj;
        }
        else if ((rho[idxi] < rho[idxj]) && (dist[idxi][idxj] == mindist) && (idxj < minidx)) {
          cl[idxi] = cl[idxj];
          mindist = dist[idxi][idxj];
          minidx = idxj;
        }
      }
    }
  }

  // Find border densities per cluster
  bord_rho = (double*) malloc((nclust + 1) * sizeof(double));
  for (i = 1; i <= nclust; i++) {
    bord_rho[i] = 0;
    for (j = 0; j < n; j++) {
      if (cl[j] == i) {
        int k;
        for (k = 0; k < n; k++) {
          if ((cl[k] != cl[j]) && (dist[j][k] <= dc) && (rho[j] > bord_rho[i]))
            bord_rho[i] = rho[j];
        }
      }
    }
  }

  // Generate Halo
  for (i = 0; i < n; i++) {
    if (rho[i] >= bord_rho[cl[i]])
      halo[i] = 0;
    else
      halo[i] = 1;
  }

  // Free structures and exit
  free(rho);
  free(sortrho);
  free(delta);
  free(sortdelta);
  free(strho);
  free(bord_rho);

  return nclust;
}
