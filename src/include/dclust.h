#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <math.h>
#include <float.h>
#include <gsl/gsl_statistics_double.h>


/*
 * Struct that handles a tuple <index><value> to sort a double array
 * by its values while keeping the original indexes
 */
typedef struct {
  double value;
  int index;
} rho_struct;


/*
 * cmpd
 *   Double comparison function for qsort
 *
 * @arg const void *x
 *   Element x to be compared
 * @arg const void *y
 *   Element y to be compared
 *
 * @return
 *   1 if x > y
 *   0 if x = y
 *  -1 if x < y
 */
int cmpd(const void *x, const void *y);


/*
 * cmpd
 *   rho_struct comparison function for qsort
 *   Inverse comparison
 *
 * @arg const void *x
 *   Element x to be compared
 * @arg const void *y
 *   Element y to be compared
 *
 * @return
 *   1 if x < y
 *   0 if x = y
 *  -1 if x > y
 */
int cmpsm (const void *x, const void *y);


/*
 * Optimization of the distance cutoff (dc) for the dclust clustering algorithm.
 *
 * dc is calculated by finding the impact factor (sigma) of a gaussian density function
 * that minimizes the value of the entropy of the potentials (H).
 *
 * @reference: Rodriguez A. and Laio A. "Clustering by fast search and find of density peaks".
 *             Science 27 June 2014
 * @reference: Wang et al. "Data Field for Hierarchical Clustering".
 *             International Journal of Data Warehousing and Mining 7(4) October 2011
 * @reference: Wang et al. "Comment on <Clustering by fast search and find of density peaks>".
 *             aRxiv:1501.04267
 *
 * @arg double** dist
 *   Distance/dissimilarity matrix
 * @arg int n
 *   Number of elements in dist
 * @arg double* max
 *   Pointer to a double variable where the maximum distance in dist will be stored
 *
 * @return
 *   Optimal distance cutoff (dc) for the dclust clustering algorithm
 */
double dcoptimize(double** dist, int n, double* max);

/*
 * Calculate a clustering by fast search and find of density peaks
 *
 * @reference: Rodriguez A. and Laio A. "Clustering by fast search and find of density peaks".
 *             Science 27 June 2014.
 *
 * @arg double** dist
 *   Distance/dissimilarity matrix
 * @arg int n
 *   Number of elements in dist
 * @arg double cutoff
 *   Distance cutoff (dc). -1 for automatic calculation.
 * @arg int gaussian
 *   1 if gaussian kernel used for density calculation. 0 otherwise.
 * @arg int* cl
 *   Cluster number per element will be stored here
 * @arg int* halo
 *   1 if element belongs to the cluster halo. 0 if element belongs to the cluster core.
 *
 * @return
 *   Number of clusters
 */
int dclust(double** dist, int n, double cutoff, int gaussian, int* cl, int* halo);
